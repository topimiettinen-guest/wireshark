commit 8fc0148955
Author: Gerald Combs <gerald@wireshark.org>
Date:   Tue Jul 16 12:18:22 2019 -0700

    Prep for 2.6.10.
    
    Change-Id: I2e127978058e848d3011c3fa399702c99f9df91f
    Reviewed-on: https://code.wireshark.org/review/33968
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 9424c89636
Author: Stig Bjørlykke <stig@bjorlykke.org>
Date:   Sun Jul 14 21:33:52 2019 +0200

    Qt: Check for NULL pointer in profile->reference
    
    Check for NULL pointer before using profile->reference in strcmp()
    because this will give a crash. Doing a copy profile from a new profile
    will set the reference to NULL in get_profile_parent().
    
    Use an unambiguously profile reference value NULL as profile->reference
    when creating a new profile to align with when copy from a new profile.
    
    Change-Id: Id61d5138ac8723e7c7ebad4d2252cf659c7569d4
    Reviewed-on: https://code.wireshark.org/review/33937
    Petri-Dish: Stig Bjørlykke <stig@bjorlykke.org>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Roland Knall <rknall@gmail.com>
    Reviewed-by: Stig Bjørlykke <stig@bjorlykke.org>
    (cherry picked from commit 5ad368fd1888963a1714d1ad36ab09219feaa926)
    Reviewed-on: https://code.wireshark.org/review/33953

commit 8088747f11
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon Jul 15 01:22:33 2019 -0700

    Clean up indentation.
    
    Change-Id: I072ae41e48583ed52679cc58fb3b65e7654bf3ab
    Reviewed-on: https://code.wireshark.org/review/33947
    Petri-Dish: Guy Harris <guy@alum.mit.edu>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 151159ad90f6561974dd4639f2a7df4a8b99cb0d)
    Reviewed-on: https://code.wireshark.org/review/33949

commit 5a6196025d
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon Jul 15 01:01:25 2019 -0700

    Fix whitespace.
    
    Change-Id: Ic30151ee08d4561740f8a27ca5f57c695dd0e19b
    Reviewed-on: https://code.wireshark.org/review/33943
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 1d4d43c2d88e039986b2b966caf2118cba2caa17)
    Reviewed-on: https://code.wireshark.org/review/33945

commit 152b16b5ee
Author: Peter Wu <peter@lekensteyn.nl>
Date:   Mon Jul 8 11:21:56 2019 +0200

    gitlab-ci: use opensuse/leap:42.3 image.
    
    The former official image has been deprecated in favor of the
    opensuse/leap one.
    
    Change-Id: If926b93bf32e6a72b3299abe0d5e34b0418d7b14
    Reviewed-on: https://code.wireshark.org/review/33878
    Reviewed-by: Dario Lombardo <lomato@gmail.com>
    Reviewed-by: Peter Wu <peter@lekensteyn.nl>

commit 6ea87c4faa
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jul 14 08:48:09 2019 +0000

    [Automatic update for 2019-07-14]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: I4f8803ec89172b2154db12973b908fe9279ef482
    Reviewed-on: https://code.wireshark.org/review/33931
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit df01e4d5bc
Author: Peter Wu <peter@lekensteyn.nl>
Date:   Sun Jul 7 13:18:06 2019 +0200

    dumpcap: fix uninitialized memory read on dumpcap -d errors
    
    Reproduce with: dumpcap -pdf bad
    
    Change-Id: I8c1f80c9d88262bc57651e886740083ea8e6ad52
    Fixes: 4d6cb744df ("Add a "-d" flag to dumpcap")
    Reviewed-on: https://code.wireshark.org/review/33863
    Petri-Dish: Peter Wu <peter@lekensteyn.nl>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit 1f527124444eca32623d1b7a5303afd7b46e322e)
    Reviewed-on: https://code.wireshark.org/review/33868
    Reviewed-by: Peter Wu <peter@lekensteyn.nl>

commit 56d6705192
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jul 7 08:46:44 2019 +0000

    [Automatic update for 2019-07-07]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: I26adeb9c1585db53a9734e8e0b04cbe1a55add79
    Reviewed-on: https://code.wireshark.org/review/33861
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 000cc11d8f
Author: Peter Wu <peter@lekensteyn.nl>
Date:   Wed Jul 3 12:11:20 2019 +0200

    Qt: fix QIcon crash on exit on Ubuntu 16.04 with Qt 5.5.1
    
    The icon used by DisplayFilterCombo is not cleaned up when MainWindow is
    destroyed, so Qt cleans it up on program exit. Due to another Qt bug
    (https://bugreports.qt.io/browse/QTBUG-50829, fixed in Qt 5.8.0), the
    QIconEngine plugin that backs the QIcon was unloaded before the cleanup
    was complete. As a result, calling the QIcon destructor would result in
    calling the unmapped QIconEngine destructor code, leading to a
    segmentation fault.
    
    master-3.0 and later are not affected since a similar fix was added via
    v2.9.0rc0-2728-gabfe9572bd.
    
    Bug: 15241
    Change-Id: Ie6bd8e645a2c754d6b57290e7084eaa324696253
    Fixes: v2.6.2rc0-170-g3a0da5fbb2 ("Qt: Create the main display filter combo sooner.")
    Reviewed-on: https://code.wireshark.org/review/33828
    Petri-Dish: Peter Wu <peter@lekensteyn.nl>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Anders Broman <a.broman58@gmail.com>

commit 6ed140178d
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon Jul 1 12:27:12 2019 -0700

    Pass the correct value to ascendlex_destroy().
    
    It takes a yyscan_t as an argument, not a pointer to a yyscan_t; a
    yyscan_t is a pointer to the scanner state.  (A pointer to it is passed
    to the init routine so that it can be set to point to the allocated
    state, not because it's a structure itself.)
    
    Change-Id: If80ca1caaa07d8a966df8d07f989b722869ac58b
    Reviewed-on: https://code.wireshark.org/review/33814
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 58cc932d2f3d7dcb55685c32a02bb7e8fec81b82)
    Reviewed-on: https://code.wireshark.org/review/33816

commit 20179f4bd8
Author: Maksim Salau <maksim.salau@gmail.com>
Date:   Thu Jun 27 21:19:33 2019 +0300

    wiretap: ascend: Destroy lexer state after parsing
    
    Lexer private structure is initialized but never destroyed or reused.
    
    Change-Id: I61d43b4cb14a2d3b3706267eb393e4562adb00f9
    Reviewed-on: https://code.wireshark.org/review/33809
    Petri-Dish: Guy Harris <guy@alum.mit.edu>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit bd5ba2ba7a6aa3fdc12d48bf9f84c2fe5f784dbb)
    Reviewed-on: https://code.wireshark.org/review/33812

commit 85ed58bf2e
Author: Pascal Quantin <pascal@wireshark.org>
Date:   Mon Jul 1 18:57:43 2019 +0200

    MAC LTE: implement 3GPP 36.321 CR 1450
    
    The NB-IoT DPR MAC CE is not included in the L field of the CCCH MAC CE.
    
    Change-Id: I497176dfc722f0080e544bbc73845cfce2064e2d
    Reviewed-on: https://code.wireshark.org/review/33805
    Petri-Dish: Pascal Quantin <pascal@wireshark.org>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Pascal Quantin <pascal@wireshark.org>
    (cherry picked from commit 3f7e6f5a7d8866d8f3f4a1ca18a7db360eab5216)
    Reviewed-on: https://code.wireshark.org/review/33807

commit 1c43f8bd2e
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon Jul 1 00:12:40 2019 -0700

    Distinguish "Interface went down" from "Interface disappeared".
    
    Have separate errors for "the interface went down" on Linux and "the
    interface no longer exists" on *BSD/Darwin/Windows.
    
    Change-Id: I1951c647e88eb7ebeb20a72d9e03a2072168c8e5
    Reviewed-on: https://code.wireshark.org/review/33794
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 37ff9dacb9e27bdf7b6b296bebad11694c6ba167)
    Reviewed-on: https://code.wireshark.org/review/33796

commit c3eaf80b0e
Author: Guy Harris <guy@alum.mit.edu>
Date:   Sun Jun 30 19:29:21 2019 -0700

    Libpcap may now say "The interface disappeared" if it did.
    
    A recent change to libpcap means that the error message if an interface
    disappears (e.g., removing a hot-pluggable device, or shutting down a
    PPP connection that was dynamically set up) is "The interface
    disappeared" rather than "The interface went down" - on FreeBSD,
    DragonFly BSD, OpenBSD, and Darwin-based OSes, capturing continues with
    no error if the interface is configured down, but either ENXIO or EIO
    (depending on the OS) is delivered if the interface disappears.
    
    Treat that error as another one to show the user without the "report
    this to the Wireshark developers" note.
    
    Change-Id: I477d87957ce30a52385f07f4b47a7824e3fca2c7
    Reviewed-on: https://code.wireshark.org/review/33790
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 8a4ce74ac9a3be8c752c1b405349d6083f76e657)
    Reviewed-on: https://code.wireshark.org/review/33792

commit 10b0b3eb47
Author: Guy Harris <guy@alum.mit.edu>
Date:   Sun Jun 30 15:58:22 2019 -0700

    Update a comment, and shuffle tests.
    
    Linux isn't the only platform where libpcap may return "The interface
    went down".
    
    Put the test for "The interface went down" first.
    
    Change-Id: I5241f0744bd12eb5e090b8e1717268bdf8392ea7
    Reviewed-on: https://code.wireshark.org/review/33785
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 03517b692b8b5cb934675da282d2452157b1aba3)
    Reviewed-on: https://code.wireshark.org/review/33788

commit bedeee579e
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jun 30 08:50:31 2019 +0000

    [Automatic update for 2019-06-30]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: Ibbad0e075a58b4a6af43bc098029114c0a8f9dcd
    Reviewed-on: https://code.wireshark.org/review/33778
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 0793039013
Author: Guy Harris <guy@alum.mit.edu>
Date:   Wed Jun 26 13:08:51 2019 -0700

    With -T, change the packet's encapsulation type as well.
    
    Bug: 15873
    Change-Id: I8d36b0fba42481b5e27e9ad9643d3603486c3645
    Reviewed-on: https://code.wireshark.org/review/33745
    Petri-Dish: Guy Harris <guy@alum.mit.edu>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit dc7b757c914761b5114954d1573aca0f6d03baae)
    Reviewed-on: https://code.wireshark.org/review/33747

commit 7e90aed666
Author: Dario Lombardo <lomato@gmail.com>
Date:   Mon Jun 24 23:36:15 2019 +0200

    asn1: don't increment a buffer beyond its end.
    
    Bug: 15870
    Change-Id: I04cbb822f0e77c8e0ac8513e3a5c13116920ca6e
    Reviewed-on: https://code.wireshark.org/review/33731
    Petri-Dish: Anders Broman <a.broman58@gmail.com>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit 45a3d0787f3c9f6f5fb5b53a8c29771b3f28e406)
    Reviewed-on: https://code.wireshark.org/review/33736
    Petri-Dish: Dario Lombardo <lomato@gmail.com>
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit eb1e1794ef
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jun 23 08:49:00 2019 +0000

    [Automatic update for 2019-06-23]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: I8f736d1236f8836809592779a2d90a134132335f
    Reviewed-on: https://code.wireshark.org/review/33716
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit b873bddfa4
Author: Guy Harris <guy@alum.mit.edu>
Date:   Wed Jun 19 16:57:40 2019 -0700

    Fix error message for an unknown pcapng version number.
    
    We were using fields in the pcapng_t that weren't set yet to report the
    version number in question; use the variables we were checking.
    
    Change-Id: Ib03bafe62d8c7b1aa54b2ef22640e3b00722142a
    Ping-Bug: 15862
    Reviewed-on: https://code.wireshark.org/review/33671
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit d6472862c5a21b82986fd6f56730c49a886fe2f5)
    Reviewed-on: https://code.wireshark.org/review/33673

commit 5cf5bee8c9
Author: Dario Lombardo <lomato@gmail.com>
Date:   Wed Jun 19 14:15:34 2019 +0200

    sshdump: fix bug in --remote-sudo.
    
    Fix documentation as well.
    
    Bug: 15845
    Change-Id: I1b4e50c21887afa6a60b76de6cc169a1d0b5067a
    Reviewed-on: https://code.wireshark.org/review/33658
    Petri-Dish: Dario Lombardo <lomato@gmail.com>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit ed34c3de14fbaed36ce3243668b28af411dfb085)
    Reviewed-on: https://code.wireshark.org/review/33663

commit a74c875e13
Author: Pascal Quantin <pascal@wireshark.org>
Date:   Tue Jun 18 20:21:00 2019 +0200

    GSM RLC/MAC: fix dissection of SI Message List IE
    
    Change-Id: Ia3a4255ecd78e480135bbbbeccd9c0268c105400
    Reviewed-on: https://code.wireshark.org/review/33648
    Petri-Dish: Pascal Quantin <pascal@wireshark.org>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Pascal Quantin <pascal@wireshark.org>
    Reviewed-on: https://code.wireshark.org/review/33650

commit 9860ad9427
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jun 16 08:52:47 2019 +0000

    [Automatic update for 2019-06-16]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: I1ec0e3558be17ee05dca3eb99679e4b1f547ddb9
    Reviewed-on: https://code.wireshark.org/review/33618
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 714640a07d
Author: Pascal Quantin <pascal@wireshark.org>
Date:   Wed Jun 12 22:46:57 2019 +0200

    Qt: fix 'open protocol preferences' shortcut for protocol subtrees
    
    Bug: 15836
    Change-Id: If0a1b6fbb7a2cf7b73ef91c07a6f1b180fc030a5
    Reviewed-on: https://code.wireshark.org/review/33570
    Petri-Dish: Pascal Quantin <pascal@wireshark.org>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Pascal Quantin <pascal@wireshark.org>
    (cherry picked from commit fb0d1ee24edeed29baac1bdb053d1553580d431c)
    Reviewed-on: https://code.wireshark.org/review/33577

commit 673f80b754
Author: Dario Lombardo <lomato@gmail.com>
Date:   Sat Jun 8 22:59:20 2019 +0200

    tshark/tfshark: fix error message.
    
    Bug: 15825
    Change-Id: Iec8dff38dd89e3947f3fe7053e38101c3ad7b1b2
    Reviewed-on: https://code.wireshark.org/review/33523
    Petri-Dish: Guy Harris <guy@alum.mit.edu>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Michael Mann <mmann78@netscape.net>
    (cherry picked from commit 5f105526af2a5023d9dab1f2a1fb912ea3922bf0)
    Reviewed-on: https://code.wireshark.org/review/33532
    Petri-Dish: Michael Mann <mmann78@netscape.net>

commit 13fda79077
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jun 9 08:52:58 2019 +0000

    [Automatic update for 2019-06-09]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: Ie3f1cfe971e19dba8de6950d0874c0426d76849e
    Reviewed-on: https://code.wireshark.org/review/33528
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 8e4ce399dc
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 15:54:42 2019 -0700

    Don't assume padding is present at the end of UNIX Info2.
    
    Check whether the byte count includes the padding before skipping it; it
    may not be present (at least not if this is at the end of the byte
    parameters).
    
    Change-Id: I4385a4713cb6813a6e8519005288d6ef5a28f028
    Reviewed-on: https://code.wireshark.org/review/33493
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 338ce1b67217e5e5f9ee81540748e34766fd85c2)
    Reviewed-on: https://code.wireshark.org/review/33495

commit 3aca4dc4ea
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 15:14:30 2019 -0700

    Fix the dissection of Find First2 Query EA Info information.
    
    The file name doesn't appear to be padded, and may have a 1-byte null
    terminator (yes, 1 byte, according to MS-CIFS) at the end, not included
    in the file name length.
    
    Change-Id: I8510434b3b5aec092290697c336924d6ff6be763
    Reviewed-on: https://code.wireshark.org/review/33486
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 782c4e496bc6d3610f504b06690e7848abd4453a)
    Reviewed-on: https://code.wireshark.org/review/33491

commit 79f6042ce1
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 14:27:12 2019 -0700

    Handle some weirdness with the primary domain field in NegProt replies.
    
    Sometimes there appears to be an extra byte before that field; try to
    catch some of those cases.
    
    Expand comments discussing various weirdness with that field, including
    a note that clients might not pay any attention to it, so maybe we just
    have buggy servers talking to clients that don't care about those
    particular bugs.
    
    Change-Id: I4d35d2e2c475d4da37debedfed31b891e6f3cfa8
    Reviewed-on: https://code.wireshark.org/review/33481
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 021e994293449ac263b0b234660847e27363a660)
    Reviewed-on: https://code.wireshark.org/review/33488

commit 49d510ff3a
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 14:44:19 2019 -0700

    Fix the dissection of create temporary file responses.
    
    According to MS-CIFS:
    
            1) the file name is not one of those "buffer format followed by
               a string" fields, it's just a string, so there's no buffer
               format field;
    
            2) it's always in ASCII, so ignore the "Unicode strings" flag.
    
    Note that, for the *request*, the *directory* name isn't claimed to
    always be ASCII, so honor the "Unicode strings" flag there.
    
    Change-Id: I495b7be8257d941ccf4b45126a44d25cf0ab2c12
    Reviewed-on: https://code.wireshark.org/review/33482
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 6259b79d257fac11cda823b7bf0e4f291d68186b)
    Reviewed-on: https://code.wireshark.org/review/33484

commit 03b8132512
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 13:02:39 2019 -0700

    Add some comments indicating what protocol was selected.
    
    Note, for all of the different word count values, what protocol or
    protocols it represents.
    
    (If we have the Negotiate request, and can thus determine which protocol
    was selected based on the set of protocols the client was willing to
    accept, should we verify that the server selected a protocol for which
    the given word count value was used, and add an expert info if it
    didn't?)
    
    Change-Id: I95ad4b1245bf2a04fdef4746815352967d8ac0a6
    Reviewed-on: https://code.wireshark.org/review/33475
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit cdaa04cc30669cb0a9272b692cf5a152ef6612c4)
    Reviewed-on: https://code.wireshark.org/review/33477

commit 53f176b0bd
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 12:46:44 2019 -0700

    Register the "missing word parameters" expert info.
    
    Change-Id: I6dbd8af61bf8ee4e55264116c1838d7bdf1b1a67
    Reviewed-on: https://code.wireshark.org/review/33468
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit c591049194a39415ea3bc25935d48b72bd7a53fb)
    Reviewed-on: https://code.wireshark.org/review/33472

commit 8abffd9929
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue Jun 4 12:44:09 2019 -0700

    Don't assume an NT Create AndX request has all the word parameters.
    
    It *should*, but a malicious or otherwise malformed packet might not
    have them.  One of them is the file name length; if it's missing, we
    can't dissect the file name, as we don't know how long it is.
    
    Change-Id: Ie259e2d8ec65f5d53d466382d89889902495d2c8
    Reviewed-on: https://code.wireshark.org/review/33467
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit f0c13af7d3862f11b47cfcb12ccc80341122395d)
    Reviewed-on: https://code.wireshark.org/review/33471

commit c839f9edf4
Author: Gerald Combs <gerald@wireshark.org>
Date:   Mon Nov 19 10:20:44 2018 -0800

    rpm-setup.sh: We no longer use FOP or AsciiDoc.
    
    Change-Id: I3b6e200be149d12a42604528a695aae98d310b9c
    Reviewed-on: https://code.wireshark.org/review/30719
    Reviewed-by: Gerald Combs <gerald@wireshark.org>
    (cherry picked from commit 8788c990b540054c512650660bf6d0f4db3df749)
    Reviewed-on: https://code.wireshark.org/review/33465

commit 8e3062b009
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun Jun 2 08:50:38 2019 +0000

    [Automatic update for 2019-06-02]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: Ic1ced8cfe7c02d79f5f92a1e40b5a89a275335ee
    Reviewed-on: https://code.wireshark.org/review/33445
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 9997eef374
Author: Jonas Jonsson <jonas@ludd.ltu.se>
Date:   Thu May 30 21:18:19 2019 +0200

    btle: Correctly detect l2cap fragment start
    
    The first L2CAP PDU fragment starts with the 4 octet long L2CAP header
    consisting of the Length and the CID fields. The Length field doesn't
    include the header itself. Thus the Length field in the BLE Data header
    will be 4 octets larger than the L2CAP PDU header Length field if the
    packet wouldn't be fragmented.
    
    The current implementation doesn't correctly detect the start fragment
    causing reassembly to fail as it compares the BLE Data Length with the
    L2CAP Length without compensating for the header.
    
    By increasing the L2CAP PDU Length field with the header length the
    reassembly works.
    
    Rename the variable to better reflect what length it actually
    represents.
    
    Bug: 15807
    Change-Id: Idcb6bdccc4daae756a63a9bae0839fe25ae99f23
    Reviewed-on: https://code.wireshark.org/review/33428
    Petri-Dish: Stig Bjørlykke <stig@bjorlykke.org>
    Reviewed-by: Stig Bjørlykke <stig@bjorlykke.org>
    (cherry picked from commit 49b6523c6cd4f8c56f428797283e150e63a52aad)
    Reviewed-on: https://code.wireshark.org/review/33429
    (cherry picked from commit 7b70ef08a0f9403c287177018c8d21a7e558cccf)
    Reviewed-on: https://code.wireshark.org/review/33430

commit c0a31ae75a
Author: Guy Harris <guy@alum.mit.edu>
Date:   Wed May 29 10:31:13 2019 -0700

    Fix handling of headers in body part.
    
    Check whether the unfolded-and-compacted header has only printable
    characters, not whether the full header does - the full header may
    include LWSP, which includes HT, CR, and LF, none of which are
    considered "printable", so valid headers were being treated as not being
    headers, causing mis-dissection of some packets.
    
    We don't need to split the header name from the value -
    is_known_multipart_header() stops comparison at the end of the header
    name.
    
    Change-Id: I96e4ac0b69df726b984ee7faeea19eda18be223c
    Reviewed-on: https://code.wireshark.org/review/33417
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    Petri-Dish: Guy Harris <guy@alum.mit.edu>
    Tested-by: Petri Dish Buildbot
    (cherry picked from commit 78a106dc2a5516b9b9cf42cf973d990828cac54e)
    Reviewed-on: https://code.wireshark.org/review/33419

commit d8e91a7a7b
Author: Guy Harris <guy@alum.mit.edu>
Date:   Tue May 28 11:03:11 2019 -0700

    Clean up indentation.
    
    Change-Id: Idfa3e15eaa1d764f66d630878f1c44561169d8bf
    Reviewed-on: https://code.wireshark.org/review/33409
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 4997002458e5262aa88c3b0d8a132d2237f909d4)
    Reviewed-on: https://code.wireshark.org/review/33411

commit 5911e56082
Author: Eric Wild <ewild@sysmocom.de>
Date:   Mon May 20 12:56:02 2019 +0200

    Fix for incorrectly decoded RSL ERR REP/BCCH INFORMATION optional IEs
    
    This patch adjusts the inconsistent usage of the is_mandatory flag
    passed to the dissect_* functions for optional IEs, which fixes the
    issue of incorrectly parsed RSL ERR REP optional IEs and the equally
    broken BCCH INFORMATION optional IE parsing.
    
    Bug: 15789
    Change-Id: I94ea8fe110d8d6aa6ebd0cec5013d3cc8fd55311
    Reviewed-on: https://code.wireshark.org/review/33269
    Reviewed-by: Harald Welte <laforge@gnumonks.org>
    Petri-Dish: Alexis La Goutte <alexis.lagoutte@gmail.com>
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit bc9f570680862c7835eb58488230b40a9e89251e)
    Reviewed-on: https://code.wireshark.org/review/33389
    Reviewed-by: Jaap Keuter <jaap.keuter@xs4all.nl>
    Petri-Dish: Jaap Keuter <jaap.keuter@xs4all.nl>
    Tested-by: Petri Dish Buildbot

commit 6077a35c05
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon May 27 18:27:44 2019 -0700

    Clean up some ASCII vs. EBCDIC string handling.
    
    In at least one capture, structure IDs are in ASCII even though the code
    page in the header is an EBCDIC code page.  Determine the structure ID's
    character encoding based on whether it's the ASCII or EBCDIC version of
    the ID value, not on the global character encoding.
    
    We were using the *integer* encoding, not the *string* encoding, for the
    "qprotect" field, which is a string; fix that.
    
    Use STR_UNICODE for strings, as they're not guaranteed to consist of
    characters that can be mapped to ASCII characters (even the common
    subset of EBCDIC, not counting code page-dependent code points, has
    non-ASCII printable characters in it).
    
    Change-Id: I971dd7ae55617c27ebe88f31089b2495374593bf
    Reviewed-on: https://code.wireshark.org/review/33399
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit b9c69d6ef8b2c759bb1b4be05240bba42038a051)
    Reviewed-on: https://code.wireshark.org/review/33401

commit 393cd686cc
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon May 27 17:46:59 2019 -0700

    Strings in mDNS TXT records are UTF-8.
    
    Change-Id: Iedde17155aae71e9bc7ad3cc5185ea33e34e209c
    Reviewed-on: https://code.wireshark.org/review/33391
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 0ac699d621ab1e033cd7b3d576b2e746932e82b8)
    Reviewed-on: https://code.wireshark.org/review/33396

commit 4336bfb1e1
Author: Guy Harris <guy@alum.mit.edu>
Date:   Mon May 27 17:43:23 2019 -0700

    Strings in the CUPS browsing protocol are UTF-8.
    
    Change-Id: I594a22acf9202f7b7ca2e4ee3c58c308c2cd7019
    Reviewed-on: https://code.wireshark.org/review/33390
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 1d88e9b25fd40a692777c9ab7fb503584afacf0d)
    Reviewed-on: https://code.wireshark.org/review/33393

commit e2692730ba
Author: Guy Harris <guy@alum.mit.edu>
Date:   Sun May 26 11:43:07 2019 -0700

    *Always* pair ENC_UTF_16 and ENC_UCS_2 with a byte order.
    
    Big-endian and little-endian UTF-16 and UCS-2 aren't the same; always
    associate them with a byte order ENC_ flag, to clarify what byte order
    is being used.  Yes, for big-endian, omitting the ENC_ flag, or using
    ENC_NA, *happens* to work, because ENC_BIG_ENDIAN and ENC_NA *happen* to
    be 0, but omitting ENC_BIG_ENDIAN doesn't make it sufficiently clear
    that it's UTF-16BE or UCS-2BE.
    
    Change-Id: Iecf7375763ce4922bd1b0676c9dc5a01731c2fec
    Reviewed-on: https://code.wireshark.org/review/33374
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 38dec96c656e438e1f09f7dda6327b85ffd0c479)
    Reviewed-on: https://code.wireshark.org/review/33376

commit d272747b65
Author: Gerald Combs <gerald@wireshark.org>
Date:   Sun May 26 08:50:12 2019 +0000

    [Automatic update for 2019-05-26]
    
    Update manuf, services enterprise numbers, translations, and other items.
    
    Change-Id: Ib303a6589882d920aae2dabdea4df8c739089b93
    Reviewed-on: https://code.wireshark.org/review/33366
    Reviewed-by: Gerald Combs <gerald@wireshark.org>

commit 6aa4428c89
Author: Guy Harris <guy@alum.mit.edu>
Date:   Fri May 24 19:32:46 2019 -0700

    *Little-endian* UTF-16.
    
    ENC_UTF_16 does *not* go with ENC_NA; ENC_NA is for cases where the byte
    order is "not applicable", such as a 1-byte number or a character
    encoding where every character is encoded in 1 byte, but UTF-16 isn't
    one of those cases, as a character is encoded in either 1 or 2 2-byte
    values.  This being a Windows thing, the byte order is little-endian.
    
    Change-Id: Iab0db3fa2c5d2c25be209e4ed0ebd57827edbcd8
    Reviewed-on: https://code.wireshark.org/review/33347
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit 2114dba1effebba50cdb611b2650b0a4a37761e4)
    Reviewed-on: https://code.wireshark.org/review/33349

commit 5a9123ecb5
Author: Guy Harris <guy@alum.mit.edu>
Date:   Fri May 24 18:51:01 2019 -0700

    "OEM Codepage" appears to mean "code page number"; show it in decimal.
    
    Code page numbers are generally referred to by their number in decimal,
    not hex.
    
    Change-Id: I1dee3df09cf7b5efaca2f4144ee5fcbc8d3ee44c
    Reviewed-on: https://code.wireshark.org/review/33343
    Reviewed-by: Guy Harris <guy@alum.mit.edu>
    (cherry picked from commit cf89939a1966c37348ca14620c0afa9ca3a23c01)
    Reviewed-on: https://code.wireshark.org/review/33345

commit 0558b2af31
Author: Jaap Keuter <jaap.keuter@xs4all.nl>
Date:   Wed May 22 21:05:41 2019 +0200

    SDP: show rtcp and rtcp-mux media attribute values
    
    With the addition of handling the rtcp and rtpc-mux media attributes
    (see cde023c3c5a08131495eb2574c00ff1f34cdce55) the default behaviour
    of presenting the media attribute value itself was lost. This change
    adds this back.
    
    Bug: 15791
    Change-Id: Ib0084b99961bfadf1d89c70b54bd4a0805f9b9f6
    Reviewed-on: https://code.wireshark.org/review/33314
    Reviewed-by: Jaap Keuter <jaap.keuter@xs4all.nl>
    Petri-Dish: Jaap Keuter <jaap.keuter@xs4all.nl>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit d36b72e6b881b773e0f54a3a8339723955a05f71)
    Reviewed-on: https://code.wireshark.org/review/33327

commit 1279b512c9
Author: Jaap Keuter <jaap.keuter@xs4all.nl>
Date:   Wed May 22 22:56:04 2019 +0200

    DPNSS: dissect Service Indicator Code synch/asynch info
    
    The DPNSS specification for the Service Indicator Code
    Synch/Asynchronous Information field states that the lower three bits of
    this field define the Data Type. This requires a filter of three bits,
    in this case 0x7, instead of 0x3 which is two bits.
    
    CID 1159107
    
    Change-Id: I38eec252c771adf085f98c3be077c9de102a37d2
    Reviewed-on: https://code.wireshark.org/review/33317
    Reviewed-by: Jaap Keuter <jaap.keuter@xs4all.nl>
    Petri-Dish: Jaap Keuter <jaap.keuter@xs4all.nl>
    Tested-by: Petri Dish Buildbot
    Reviewed-by: Anders Broman <a.broman58@gmail.com>
    (cherry picked from commit 6cb990ccb03e3fa4154e080d8592f64ca45a9c7b)
    Reviewed-on: https://code.wireshark.org/review/33324

commit 35bf3bcaeb
Author: Gerald Combs <gerald@wireshark.org>
Date:   Wed May 22 14:30:16 2019 -0700

    2.6.9 → 2.6.10.
    
    Change-Id: I283dd938ac462933dc473c61a776fcfa9ef51e38
    Reviewed-on: https://code.wireshark.org/review/33319
    Reviewed-by: Gerald Combs <gerald@wireshark.org>
